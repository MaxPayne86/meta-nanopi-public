SUMMARY = "Include file nanohat oled display image"
HOMEPAGE = "http://www.foo.com"
LICENSE = "CLOSED"

inherit core-image

CORE_OS = " \
    openssh openssh-keygen openssh-sftp-server \
    tzdata \
    lsof \
    procps \
    util-linux \
    coreutils \
    ldd \
    swig \
    zlib \
    rng-tools \
    cpufrequtils \
    i2c-tools \
    dnsmasq \
"

PYTHONSTUFF = " \
    python3-pip \
    python-setuptools \
    python-cython \
    python3-numpy \
    python3-pyserial \
    python3-tornado \
    python-psutil \
    python-modules \
    python3-modules \
    python3-pydoc \
    python3-smbus \
    python3-requests \
    python3-pillow \
"

WIFI_SUPPORT = " \
    crda \
    iw \
    linux-firmware-brcm43430 \
    linux-firmware-ralink \
    linux-firmware-rtl8192ce \
    linux-firmware-rtl8192cu \
    linux-firmware-rtl8192su \
    wireless-tools \
    wpa-supplicant \
"

AP_TOOLS = " \
    hostapd \
    iptables \
"

DEV_SDK_INSTALL = " \
    binutils \
    binutils-symlinks \
    cpp \
    cpp-symlinks \
    diffutils \
    file \
    g++ \
    g++-symlinks \
    gcc \
    gcc-symlinks \
    gdb \
    gdbserver \
    gettext \
    git \
    ldd \
    libstdc++ \
    libstdc++-dev \
    libtool \
    make \
    perl-modules \
    pkgconfig \
"

DEV_EXTRAS = " \
    ntp \
    ntp-tickadj \
    serialecho  \
    spiloop \
"

EXTRA_TOOLS_INSTALL = " \
    bzip2 \
    devmem2 \
    dosfstools \
    ethtool \
    fbset \
    findutils \
    iperf \
    iproute2 \
    less \
    memtester \
    nano \
    netcat \
    procps \
    rsync \
    sysfsutils \
    tcpdump \
    unzip \
    util-linux \
    wget \
    zip \
"

CAN_TOOLS = " \
    canutils \
"

NANOPI_STUFF = " \
    rpi-gpio \
    gpio \
    bakebit \
"

HMI_STUFF = " \
    hmi \
"

RT_PREEMPT_STUFF = " \
    rt-tests \
"
