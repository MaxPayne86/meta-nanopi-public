SUMMARY = "An image with friendlyelec nanohat oled support"
HOMEPAGE = "http://www.foo.com"
LICENSE = "CLOSED"

IMAGE_LINGUAS = "en-us"

require nanohatoled.inc

IMAGE_INSTALL += " \
    ${CORE_OS} \
    ${RT_PREEMPT_STUFF} \
    ${PYTHONSTUFF} \
    ${NANOPI_STUFF} \
    ${HMI_STUFF} \
"

set_local_timezone() {
    ln -sf /usr/share/zoneinfo/EST5EDT ${IMAGE_ROOTFS}/etc/localtime
}

disable_bootlogd() {
    echo BOOTLOGD_ENABLE=no > ${IMAGE_ROOTFS}/etc/default/bootlogd
}

ROOTFS_POSTPROCESS_COMMAND += " \
    set_local_timezone ; \
    disable_bootlogd ; \
"

IMAGE_ROOTFS_EXTRA_SPACE = "51200"

export IMAGE_BASENAME = "nanohatoled-image"
