DESCRIPTION="Upstream's U-boot configured for sunxi devices"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

COMPATIBLE_MACHINE = "(sun4i|sun5i|sun7i|sun8i|sun50i)"

SRC_URI += "\
    file://0001-Disabling-features-in-u-boot-trying-to-speed-up-boot.patch \
    file://0002-Ensure-that-if-the-command-aren-t-enabled-we-don-t-t.patch \
    file://0003-Call-eth-function-only-if-feature-is-enabled.patch \
    file://0004-u-boot-size-is-undefined-if-environment-is-not-enabled.patch \
"
