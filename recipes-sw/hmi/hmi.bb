DESCRIPTION = "HMI software developed for the embedded course"
HOMEPAGE = "https://bitbucket.org/MaxPayne86/hmi-oled/src/embedded-programming-course"
SECTION = "devel/python"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

do_configure[noexec] = "1"
do_compile[noexec] = "1"

SRC_URI = "\
    git://MaxPayne86@bitbucket.org/MaxPayne86/hmi-oled.git;protocol=https;branch=embedded-programming-course \
"
SRCREV="embedded-programming-course"

do_install () {
	# Install python hmi scripts folder
	install -d ${D}/usr/local/hmi
	install -m 755 ${WORKDIR}/git/Python/* ${D}/usr/local/hmi
}

FILES_${PN} = "\
	/usr/local/hmi \
"
