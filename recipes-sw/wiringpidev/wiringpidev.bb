DESCRIPTION = "This is a GPIO access library for NanoPI."
SECTION = "libs"
HOMEPAGE = "https://github.com/friendlyarm/WiringNP"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

INSANE_SKIP_${PN} = "ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
INHIBIT_PACKAGE_DEBUG_SPLIT  = "1"
PV = "2"
BUILD_NUMBER = "0"

SRC_URI = "\
    git://github.com/friendlyarm/WiringNP.git;protocol=git;branch=master \
"
SRCREV="master"

S = "${WORKDIR}/git/devLib"

do_compile () {
    oe_runmake 'CC=${CC}'
}

do_install () {
    install -m 0755 -d ${D}${libdir}
    install -m 0755 -d ${D}${includedir}
    oe_soinstall ${S}/libwiringPiDev.so.${PV}.${BUILD_NUMBER} ${D}${libdir}
    install -m 0755 ${S}/*.h ${D}${includedir}
}

DEPENDS = "\
    wiringpi \
"
