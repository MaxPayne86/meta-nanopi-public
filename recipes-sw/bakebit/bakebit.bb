DESCRIPTION = "BakeBit is an open source platform for connecting BakeBit Sensors to the NanoPi NEO/NEO2. It is based on the GrovePi. \
Currently supported boards: NanoPi NEO, NanoPi NEO2, NanoPi NEO Air."
HOMEPAGE = "https://github.com/friendlyarm/BakeBit.git"
SECTION = "devel/python"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

SRC_URI = "\
	git://github.com/MaxPayne86/BakeBit.git;protocol=git;branch=master \
"
SRCREV="master"

S = "${WORKDIR}/git/Software/Python"

inherit setuptools3
