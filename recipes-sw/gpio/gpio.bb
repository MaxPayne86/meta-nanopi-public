DESCRIPTION = "This is a GPIO access library for NanoPI."
SECTION = "console/utils"
HOMEPAGE = "https://github.com/friendlyarm/WiringNP"
LICENSE = "CLOSED"
LIC_FILES_CHKSUM = ""

INSANE_SKIP_${PN} = "ldflags"

SRC_URI = "\
    git://github.com/friendlyarm/WiringNP.git;protocol=git;branch=master \
"
SRCREV="master"

S = "${WORKDIR}/git/gpio"

do_compile () {
    oe_runmake DESTDIR=${D}/usr PREFIX=/local 'CC=${CC}'
}

do_install () {
    install -d ${D}${bindir}
    install -m 755 ${S}/gpio ${D}${bindir}
}

DEPENDS = "\
    wiringpi \
    wiringpidev \
"

RDEPENDS_${PN} = "\
    wiringpi \
    wiringpidev \
"

FILES_${PN} += "\
    ${bindir} \
"

FILES_${PN}-dbg += "${bindir}/.debug"
