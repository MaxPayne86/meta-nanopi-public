DESCRIPTION = "A module to control Raspberry Pi GPIO channels"
HOMEPAGE = "http://code.google.com/p/raspberry-gpio-python/"
SECTION = "devel/python"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://LICENCE.txt;md5=9b95630a648966b142f1a0dcea001cb7"
FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

PV = "0.7.0"

SRCNAME = "RPi.GPIO"

SRC_URI = "\
          https://pypi.python.org/packages/source/R/RPi.GPIO/${SRCNAME}-${PV}.tar.gz \
          file://unproper-fix-cpuinfo-issue-on-aarch64.patch \
          file://revert-init-to-old-function-that-works.patch \
          "
S = "${WORKDIR}/${SRCNAME}-${PV}"

inherit setuptools3

COMPATIBLE_MACHINE = "nanopi-neo2"

SRC_URI[md5sum] = "777617f9dea9a1680f9af43db0cf150e"
SRC_URI[sha256sum] = "7424bc6c205466764f30f666c18187a0824077daf20b295c42f08aea2cb87d3f"
