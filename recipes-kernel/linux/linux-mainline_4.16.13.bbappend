SECTION = "kernel"
DESCRIPTION = "Mainline Linux kernel"
LICENSE = "GPLv2"
#LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"
COMPATIBLE_MACHINE = "(sun4i|sun5i|sun7i|sun8i|sun50i)"

FILESEXTRAPATHS_prepend := "${THISDIR}/linux-mainline:"

SRC_URI += " \
    file://patch-4.16.15-rt6.patch \
    file://patch-enable_i2c0.patch \
"

